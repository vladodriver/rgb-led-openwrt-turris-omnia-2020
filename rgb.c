#include <dirent.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SYS_PATH                                                               \
  "/sys/devices/platform/soc/soc:internal-regs/f1011000.i2c/i2c-0/i2c-1/"      \
  "1-002b/"
#define LD "leds"
#define COLORDEV "multi_intensity"

// represent 24 bit RGB color
typedef struct RGB {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} RGB;

// type for callback runs on all rgb diodes nargs - number of additional args
typedef void (*action_cb)(int nargs, ...);

// convert hex RGB value string to RGB struct
struct RGB hex_to_rgb(const char *hexstr) {
  int r, g, b;
  sscanf(hexstr, "%02x%02x%02x", &r, &g, &b);
  struct RGB rgb;
  rgb.r = r;
  rgb.g = g;
  rgb.b = b;
  return rgb;
}

// check is 6char length hex format return 1 - true
int is_rgb_hex(char *hex) {
  int n = strlen(hex);
  if (n != 6)
    return 0;
  for (int i = 0; i < n; i++) {
    char ch = hex[i];
    if ((ch < '0' || ch > '9') && (ch < 'A' || ch > 'F') &&
        (ch < 'a' || ch > 'f')) {
      return 0;
    }
  }
  return 1;
}

// print RGB value as ANSI rgb color in console
void color_print(RGB rgb) {
  printf("\x1b[38;2;%d;%d;%dm%s\x1b[0m", rgb.r, rgb.g, rgb.b, "▣");
}

// concat SYS_PATH + LD + devname + COLORDEV
char *get_devpath(char *devname) {
  char leds_dir[] = SYS_PATH LD;
  char *devpath =
      malloc(strlen(leds_dir) + strlen(devname) + strlen(COLORDEV) + 3);
  if (devpath == NULL) {
    printf("Path allocation error\n");
    return "";
  }
  sprintf(devpath, "%s/%s/%s", leds_dir, devname, COLORDEV);
  return devpath;
}

// get RGB value from device or restore file line (from_restore = 1)
RGB get_rgb(char *dn, int from_restore) {
  char *line = NULL;
  if (from_restore) {
    line = dn;
  } else {
    char *dev = get_devpath(dn);
    FILE *fp = fopen(dev, "r");
    free(dev);
    if (fp == NULL) {
      printf("Failed to open file: '%s'\n", dn);
      exit(EXIT_FAILURE);
    }

    size_t read;
    size_t len = 0;
    if ((read = getline(&line, &len, fp) == -1)) {
      printf("Can not get data form file: '%s'\n", dn);
      exit(EXIT_FAILURE);
    }
  }

  RGB rgb;
  char *r = strtok(line, " ");
  rgb.r = (unsigned char)atoi(r);
  char *g = strtok(NULL, " ");
  rgb.g = (unsigned char)atoi(g);
  char *b = strtok(NULL, " ");
  rgb.b = (unsigned char)atoi(b);
  return rgb;
}

// set rgb color on device dn to rgbhex (RGB color in 6-char hex)
void set_color(char *dn, char *rgbhex) {
  if (!is_rgb_hex(rgbhex)) {
    printf("Invalid hex color format (6-char in hex rgb)\n");
    exit(EXIT_FAILURE);
  }

  char *dev = get_devpath(dn);
  FILE *fp = fopen(dev, "w");
  free(dev);
  if (fp == NULL) {
    printf("Failed to open and write RGB color to device: '%s'\n", dn);
    exit(EXIT_FAILURE);
  }

  RGB rgb = hex_to_rgb(rgbhex);
  fprintf(fp, "%d %d %d", rgb.r, rgb.g, rgb.b);
  fclose(fp);
}

// restore all saved rgb colors from restore file
void restore(char *rfile_path) {
  FILE *rfp = fopen(rfile_path, "r");
  if (rfp == NULL) {
    printf("Could not open file: %s\n", rfile_path);
    exit(EXIT_FAILURE);
  }
  char *line = NULL;
  size_t read = 0;
  size_t len = 0;
  while ((read = getline(&line, &len, rfp)) != -1) {
    char *dev = strtok(line, " ");  // skip device name
    char *cline = strtok(NULL, ""); // get remained line part
    RGB rgb = get_rgb(cline, 1);
    char hex[7];
    sprintf(hex, "%02x%02x%02x", rgb.r, rgb.g, rgb.b);
    printf("Restore color dev: %s to R: %d G: %d B: %d, hexrgb:%s\n", dev,
           rgb.r, rgb.g, rgb.b, hex);
    set_color(dev, hex);
  }
  fclose(rfp);
}

// get value of intensity (brightness) in %
int get_intensity() {
  const char *dp = SYS_PATH "/brightness";
  FILE *fp = fopen(dp, "r");
  if (fp == NULL) {
    printf("Failed to open and read file: '%s'\n", dp);
    exit(EXIT_FAILURE);
  }

  char *line = NULL;
  size_t len = 0;
  size_t read;
  int intensity;
  if ((read = getline(&line, &len, fp) != -1)) {
    intensity = atoi(line);
  } else {
    printf("Can not get any data form file: '%s'\n", dp);
  }
  fclose(fp);
  return intensity;
}

// set intensity value (in %) of all diodes (brightness)
void set_intensity(int percent) {
  const char *dp = SYS_PATH "/brightness";
  FILE *fp = fopen(dp, "w");
  if (fp == NULL) {
    printf("Failed to open and write to file: '%s'\n", dp);
    exit(EXIT_FAILURE);
  }
  fprintf(fp, "%d", percent);
  fclose(fp);
}

// run one action of all rgb diodes (*data can be NULL)
void do_action_all(action_cb acb, char *data) {
  char leds_path[] = SYS_PATH LD;
  struct dirent *de;
  DIR *dr = opendir(leds_path);

  if (dr == NULL) {
    printf("Could not open \"%s\" directory\n", leds_path);
    exit(EXIT_FAILURE);
  }

  while ((de = readdir(dr)) != NULL)
    if (!(!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))) {
      if (data == NULL) {
        acb(1, de->d_name);
      } else {
        acb(2, de->d_name, data);
      }
    }
  closedir(dr);
}

// action for visual show all rgb diodes colors using ANSI escape code
void show(int argn, ...) {
  va_list args;
  va_start(args, argn);
  char *dn = va_arg(args, char *);
  va_end(args);

  RGB rgb = get_rgb(dn, 0);
  color_print(rgb);
  printf(" => %s\n", dn);
}

// action for show all rgb settings in format for save into restore file
void save(int argn, ...) {
  va_list args;
  va_start(args, argn);
  char *dn = va_arg(args, char *);
  va_end(args);

  RGB rgb = get_rgb(dn, 0);
  printf("%s %d %d %d\n", dn, rgb.r, rgb.g, rgb.b);
}

// action for set all to same color(varg2 - *data)
void set_color_all(int argn, ...) {
  va_list args;
  va_start(args, argn);
  char *dn = va_arg(args, char *);  // device
  char *hex = va_arg(args, char *); // color
  va_end(args);
  set_color(dn, hex);
}

// print Usage help message
void help(char *name) {
  const char *usage =
      "\nUsage:\n\n"
      "Must provide valid command: [color, restore, save, reset, intensity]\n\n"
      "%1$s show //show all current color in console (need terminal "
      "supported)\n"
      "%1$s color <DEV_NAME or 'all'> <HEX_COLOR (<rr><gg><bb>)>\n"
      "%1$s restore <FILENAME_PATH>\n"
      "%1$s reset\n"
      "%1$s intensity <number 0 - 100>\n"
      "%1$s save > <ANY_FILENAME>\n"
      "\nFor valid devices use show command:\n";

  printf(usage, name);
  do_action_all(show, NULL);
}

int main(int narg, char *varg[]) {
  if (narg > 1) {
    if (!strcmp(varg[1], "show")) {
      do_action_all(show, NULL);
      printf("Intensity (brightness) of all rgb leds is: \x1b[1m%d\x1b[0m%%\n",
             get_intensity());
    } else if (!strcmp(varg[1], "color")) {
      if (narg > 3) {
        if (!strcmp(varg[2], "all")) {
          do_action_all(set_color_all, varg[3]);
        } else {
          set_color(varg[2], varg[3]);
        }
      } else {
        printf("Must provide valid device name and color in 6-char RGB hex "
               "format\n");
        help(varg[0]);
      }
    } else if (!strcmp(varg[1], "restore")) {
      if (narg > 2) {
        restore(varg[2]);
      } else {
        printf("Must provide path to restore file as next argument\n");
        help(varg[0]);
      }
    } else if (!strcmp(varg[1], "reset")) {
      do_action_all(set_color_all, "ffffff");
    } else if (!strcmp(varg[1], "intensity")) {
      int intensity = 0;
      if ((narg > 2) && (intensity > -1) && (intensity < 101)) {
        intensity = atoi(varg[2]);
        set_intensity(intensity);
      } else {
        printf(
            "Must provide brightness value 0 - 100 (in %%) as next argument\n");
        help(varg[0]);
      }
    } else if (!strcmp(varg[1], "save")) {
      do_action_all(save, NULL);
    } else {
      help(varg[0]);
    }
  } else {
    help(varg[0]);
  }
}
