#!/usr/bin/env ash
# This reference implementation is prototype for rgb.c (main implementation)
SYS_PATH="/sys/devices/platform/soc/soc:internal-regs/f1011000.i2c/i2c-0/i2c-1/1-002b"
LD="leds"

hex_to_dec_num() {
    #convert [0-9a-f] one char to number 0-15
    char="$1"
    d='^[0-9]$'
    h='^[a-fA-F]$'
    if ! [ $char ]; then
        return
    elif [[ $char =~ $d  ]]; then
        printf $char
     elif [[ $char =~ $h ]]; then
        CR="abcdefABCDEF"
        for i in $(seq 0 6); do
            s=${CR:"$i":1} #small letter
            l=${CR:"$((i+6))":1} #large letter
            if [ $s == $char ] || [ $l == $char ]; then
                printf "$((i + 10))"
                return
            fi
        done
    fi
}

byte_to_dec() {
    # convert 00 - ff one RGB byte to number 0 - 256
    b=$1
    f=${b:0:1}
    l=${b:1:1}
    fd=$(hex_to_dec_num $f)
    ld=$(hex_to_dec_num $l)
    printf "%d" $(((ld*1) + (fd*16)))
}

hex_to_rgb() {
    # convert RGB color in hex format to <0-255> <0-255> <0-255> need for sysfs
    hv=$1
    hr=${hv:0:2}
    hg=${hv:2:2}
    hb=${hv:4:2}
    rgbhex='^[0-9a-fA-F]{6}$'
    if [[ $hv =~ $rgbhex ]]; then
        printf "%s %s %s" $(byte_to_dec $hr) $(byte_to_dec $hg) $(byte_to_dec $hb)
    else
        printf ""
    fi
}

color_print() {
    # ANSI color printed from rgb values to: ▣
    printf "\x1b[38;2;${1};${2};${3}m▣\x1b[0m"
}

show() {
    printf "Colors of RGB diodes:\n"
    #show diodes color isong ASCI RGB color in console (must be supported by terminal ex.: konsole xterm)
    for diode in $SYS_PATH/$LD/*; do
        dev=$(basename $diode)
        rgb=$(cat $diode/multi_intensity)
        r=$(echo $rgb | awk '{print $1}')
        g=$(echo $rgb | awk '{print $2}')
        b=$(echo $rgb | awk '{print $3}')
        printf "%s => ${dev}\n" "$(color_print $r $g $b)"
    done
        printf "Intensity (brightness) of all rgb leds is: \x1b[1m%s%%\x1b[0m\n" "$(cat $SYS_PATH/brightness)"
}

restore() {
    #restore settings from file saved using by: rgb.sh save > file
    file=$1
    while read -r line; do
        diode=$(echo $line | awk '{print $1}')
        rgb_dev="$SYS_PATH/$LD/$diode/multi_intensity"
        if [ -w $rgb_dev ]; then
            # printf "$rgb_dev\n"
            r=$(echo $line | awk '{print $2}')
            g=$(echo $line | awk '{print $3}')
            b=$(echo $line | awk '{print $4}')
            printf '%s %s %s\n' "$r" "$g" "$b" > $rgb_dev
        else
            printf "Error: device: %s is not be writable!\n" "$rgb_dev"
        fi
    done < $file
}

reset() {
    # set all diodes to white color
    color=$1
    for diode in $SYS_PATH/$LD/*; do
        printf "255 255 255" > "$diode/multi_intensity"
    done
}

intensity() {
    # set intensity (brightness) of all diodes 0 is off, maximum is 100
    val=$1
    isnum='^[0-9]+$'
    if [[ $val =~ $isnum ]] && [ $val -lt 101 ] && [ $val -gt -1 ]; then
        printf $val > "$SYS_PATH/brightness"
    else
        printf "Error value for bightness must be in range 0 - 100 !\n"
    fi
}

save() {
    # save print all rgb diodes settings (printed to stdout) ex: rgb.sh save > myfile
    file=$1
    for diode in $SYS_PATH/$LD/*; do
        printf "%s %s\n" "$(basename $diode)" "$(cat "$diode/multi_intensity")"
    done
}

color() {
    # write new color ($2) to sysfs dev name ($1)
    DEV=$1
    hex_rgb=$2
    RGB=$(hex_to_rgb "$hex_rgb")
    if [ "$RGB" == "" ]; then
        printf "Error: '%s' is not valid 6 char hex RGB!\n" $hex_rgb
        return
    fi
    if [ "$DEV" == "all" ]; then
        printf "Set all diodes to RGB: ${RGB}\n"
        for diode in $SYS_PATH/$LD/*; do
            printf "$RGB" > "$SYS_PATH/$LD/$(basename $diode)/multi_intensity"
        done
        return
    fi
    printf "RGB: '%s'\n" $RGB
    if [ -w "$SYS_PATH/$LD/$DEV/multi_intensity" ]; then
        printf "$RGB" > "$SYS_PATH/$LD/$DEV/multi_intensity"
    else
        printf "Error sysfs device: \"$SYS_PATH/$LD/$DEV/multi_intensity\" must exists and must be writable!\n"
    fi
}

get_diodes() {
    # list all dev names of all RGB diodes
    diodes=""
    for diode in $SYS_PATH/$LD/*; do
        diodes="$diodes $(basename $diode)\n"
    done
    printf "$diodes"
}

cmd() {
    # main cmd switch
    case $1 in
        "show")
            show
            ;;
        "color")
            color $2 $3
            ;;
        "restore")
            printf "Restore colors from: %s\n" "$2"
            restore $2
            ;;
        "save")
            save
            ;;
        "reset")
            reset
            ;;
        "intensity")
            intensity $2
            ;;
        *)
            printf "\nUsage\n\n"
            printf "Must provide valid command: [color, restore, save, reset, intensity]\n\n"
            printf "rgb.sh show //show all current color in console (need terminal supported)\n"
            printf "rgb.sh color <DEV_NAME or 'all'> <HEX_COLOR (<rr><gg><bb>)>\n"
            printf "rgb.sh restore <FILENAME_PATH>\n"
            printf "rgb.sh reset\n"
            printf "rgb.sh intensity <number 0 - 100>\n"
            printf "rgb.sh save > <ANY_FILENAME>\n"
            printf "\nValid devices are:\n\n$(get_diodes)\n\n"
            ;;
    esac
}

cmd $@
